#!/bin/bash

git switch base -C working

for b in mysql laravel-mysql ; do
	git cherry-pick origin/$b
	git commit --amend --reset-author --no-edit
	git branch -f $b
done

git switch base -C working

for b in postgres laravel-postgres ; do
	git cherry-pick origin/$b
	git commit --amend --reset-author --no-edit
	git branch -f $b
done

git switch base -C demo
git merge mysql postgres --no-edit

git switch base
git branch -D working
