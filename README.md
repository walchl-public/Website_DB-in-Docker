# Build a Web Site in Docker with Databases


### Open Services
``` shell
docker-compose up --build
```

### Open Service Daemons
``` shell
docker-compose up --build -d
```

### Close Service Daemons
``` shell
docker-compose down
```

### Rebuild (web) image
``` shell
docker-compose build
```

### Clean Directories
``` shell
sudo git clean -df
```


### Notes
+ Databases
	+ Choose **Character Sets** for rare Chinese names & **emoji** capabilities (and sorting performance)
		+ **PostgreSQL**
			+ The default **utf8** already solve everything.

				![](https://img.gifmagazine.net/gifmagazine/images/4202856/medium_hq.gif)
	<!-- MERGE_ANCHOR -->
		+ **MySQL**
			+ Use at least **utf8mb4_unicode_ci** for database encoding.
				+ 8.0.1+:
					+ default: **utf8mb4_0900_ai_ci**
				+ 5.7.7+ (2015-04-08):
					+ suggest: **utf8mb4_unicode_ci**
					+ support: **utf8mb4_unicode_520_ci**
				+ Before 5.7.7:
					+ suggest: **utf8mb4_unicode_ci**

				![](https://img.gifmagazine.net/gifmagazine/images/4202864/medium_hq.gif)
	+ Web Admin:
		+ /adminer.php
	+ Link database in php by **PDO**.
		+ The **PHP Data Objects** maintain identical interfaces for different databases.

			![](https://img.gifmagazine.net/gifmagazine/images/4202857/medium_hq.gif)
		+ **PostgreSQL**
			+ localhost(by unix socket):
				+ /var/run/postgresql/.s.PGSQL.5432
				+ By port:
					+ db_postgres:5432
		<!-- MERGE_ANCHOR -->
		+ **MySQL**
			+ MySQL Protocol:
				+ localhost(by unix socket):
					+ /var/run/mysqld/mysqld.sock
				+ By port:
					+ db_mysql:3306
+ Web Server
	+ Add files in **./html/subdir**.
	+ Databases are already linked by unix sockets.
		+ **localhost**
